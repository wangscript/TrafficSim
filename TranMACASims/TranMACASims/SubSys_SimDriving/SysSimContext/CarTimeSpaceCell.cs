using SubSys_SimDriving.SysSimContext;
using SubSys_SimDriving;

namespace SubSys_SimDriving.SysSimContext
{
	internal class CarTimeSpaceCell
	{
		internal int iCarModelID;
		 
		internal int iSpeed;
		 
		internal int iTimeStep;
		 
		internal SimContext simDrivingStaticTable;
		 
	}
	 
}
 
